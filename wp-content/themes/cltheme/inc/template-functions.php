<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package cltheme
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function cltheme_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'cltheme_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function cltheme_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'cltheme_pingback_header' );

/**
* fix wp logo to customer logo, link logo page login
 */
function custom_loginlogo() {
	echo '<style type="text/css">h1 a {background-image: url('.get_template_directory_uri().'/assets/images/logo.png) !important; height: 100px !important; width: 100% !important; background-size: contain !important; margin-bottom: 10px !important; } #loginform{ margin-top: 0px !important}</style>';
	}
add_action('login_head', 'custom_loginlogo');
	
add_filter( 'login_headerurl', 'my_custom_login_url' );
function my_custom_login_url($url) {
	$link = get_home_url();
	return $link;
}

/**
* Add Recommended size image to Featured Image Box	  
 */
function vr_add_featured_image_instruction( $html ) {
	if(get_post_type() === 'post') {
		$html .= '<p>Recommended size: 400×260</p>';
   	}
	if(get_post_type() === 'event') {
		$html .= '<p>Recommended size: 920x360</p>';
	}
	
	return $html;
}
add_filter( 'admin_post_thumbnail_html', 'vr_add_featured_image_instruction');

/**
* remove the comment menu for people who don't have administrator rights
 */
if( !current_user_can( 'administrator' ) ){
    add_action( 'admin_init', function () {
        remove_menu_page( 'edit-comments.php' );
    });
}
