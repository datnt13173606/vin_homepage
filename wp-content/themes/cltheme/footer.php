<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cltheme
 */

?>

<footer class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-12 col-lg-3">
                    <div class="d-flex mb-3">
                        <a class="logo-ft" href="<?php echo get_home_url(); ?>">
                            <img class="img-fluid"
                                src="<?php echo get_template_directory_uri(); ?>/assets/images/logo_footer.png"
                                alt="<?php echo get_option('blogname'); ?>">
                        </a>
                    </div>
                </div>
                <div class="col-6 col-lg-3">
                    <div class="wg-ft">
                        <div class="wg-ft mb-4">
                            <?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'menu-footer-1',
                                    'menu_class' => 'no-bullets',
                                    'container' => ''
                                )
                            );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-3">
                    <div class="wg-ft">
                        <div class="wg-ft mb-4">
                            <?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'menu-footer-2',
                                    'menu_class' => 'no-bullets',
                                    'container' => ''
                                )
                            );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-3">
                    <h5 class="text-white h6">Contact</h5>
                    <p class="text-white">VinUniversity, Vinhomes Ocean Park, Gia Lam District, Hanoi.</p>
                    <h6 class="text-contact text-white">support@example.com </h6>
                    <h6 class="text-contact text-white">(84) 555-0120-111 </h6>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="copyright-content">
                <p>
                    <?php
                    echo __('Copyright © 2022 VinUni. All right reserved ', 'cltheme');
                    ?>
                </p>
            </div>
        </div>
    </div>
</footer>

<footer class="footer d-none">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="d-flex mb-3">
                        <a class="logo-ft" href="<?php echo get_home_url(); ?>">
                            <img class="img-fluid" width="90" height="76"
                                src="<?php echo get_template_directory_uri(); ?>/assets/images/logo_footer.png"
                                alt="<?php echo get_option('blogname'); ?>">
                        </a>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="wg-ft">
                        <h5 class="h6 mb-3"><?php echo __('Contact Us', 'cltheme'); ?></h5>

                        <div class="wg-ft mb-4">
                            <?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'menu-footer-1',
                                    'menu_class' => 'no-bullets',
                                    'container' => ''
                                )
                            );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="wg-ft">
                        <h5 class="h6 mb-3"><?php echo __('Follow Us ', 'cltheme'); ?></h5>

                        <div class="wg-ft mb-4">
                            <?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'menu-footer-2',
                                    'menu_class' => 'no-bullets',
                                    'container' => ''
                                )
                            );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="copyright-content">
                <p>
                    <?php
                    echo __('Copyright © 2022 VinUni. All right reserved ', 'cltheme');
                    ?>
                </p>
            </div>
        </div>
    </div>
</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>